my_grid = [[(), (3,1), (1,1,1), (1,1,1), (1,1,1), (1,3)],
           [(5), [0], [0], [0], [0], [0]],
           [(1), [0], [0], [0], [0], [0]],
           [(5), [0], [0], [0], [0], [0]],
           [(1), [0], [0], [0], [0], [0]],
           [(5), [0], [0], [0], [0], [0]]]
def column_clues(my_grid):
    return my_grid[0]
def row_clues(my_grid):
    return [lst[0] for lst in my_grid]
def sure_coloring_column(my_grid):
    special_cases = {(5): [[1], [1], [1], [1], [1]], (1,3): [[1], [0], [1], [1], [1]],  (2,2): [[1], [1], [0], [1], [1]], (3,1): [[1], [1], [1], [0], [1]], (1,1,1): [[1], [0], [1], [0], [1]], (4):[[0], [1], [1], [1], [0]], (3): [[0], [0], [1], [0], [0]]}
    for pos, column_clue in enumerate(column_clues(my_grid)):
        if column_clue in special_cases.keys():
            i = 0
            for row in my_grid[1:]:
                row[pos] = special_cases[column_clue][i]
                i += 1
    return my_grid
def sure_coloring_row(my_grid):
    special_cases = {(5): [[1], [1], [1], [1], [1]], (1,3): [[1], [0], [1], [1], [1]],  (2,2): [[1], [1], [0], [1], [1]], (3,1): [[1], [1], [1], [0], [1]], (1,1,1): [[1], [0], [1], [0], [1]], (4):[[0], [1], [1], [1], [0]], (3): [[0], [0], [1], [0], [0]]}
    for pos, row in enumerate(my_grid[1:]):
        if row[0] in special_cases.keys():
            ref_lst = my_grid[pos + 1]
            my_grid[pos + 1] = [(row[0])]
            for pos_, ele in enumerate(special_cases[row[0]]):
                if ref_lst[pos_ + 1] != [1]:
                    my_grid[pos + 1].append([1])
                my_grid[pos + 1].append(ele)
    return my_grid

sure_coloring_column(my_grid)
sure_coloring_row(my_grid)
