.. Nonogram documentation master file, created by
   sphinx-quickstart on Wed May 31 12:32:44 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Nonogram's documentation!
====================================

.. toctree::
   intro

Our Project
-----------
Problem Statement
=====================


Source Code
===========
Functions we have defined -

.. toctree::
   column_color_index
   row_color_index
   sure_coloring


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
