sure_coloring(my_grid)
==========================

This function updates the grid by coloring the cells for some special cases.
We observe that the coloring is fixed for the below special cases.

[5] -> [[1],[1],[1],[1],[1]]
[1,3] -> [[1],[0],[1],[1],[1]]
[2,2] -> [[1],[1],[0],[1],[1]]


