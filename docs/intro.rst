Introduction
============

Welcome to Nonogram's Documentation!!
In this project, we develop a computer program that can solve nonograms given by the user efficiently.

What are Nonograms?
-------------------

Nonograms are logical puzzles in which we are supposed to color the cells of a grid based on the numerical clues given for each row and column.

Example
-------

Consider this nonogram puzzle.

.. image:: /nonogram_eg.png
  :alt: Nonogram Example 
   
The solution to the above nonogram puzzle is -

.. image:: /nonogram_soln.png
  :alt: Nonogram Solution

